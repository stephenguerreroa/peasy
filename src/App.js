
import Sorteo from './Sorteo';
import './App.css';

function App() {
  return (
    <div className='App-header'>
      <Sorteo />
    </div>
  );
}

export default App;
